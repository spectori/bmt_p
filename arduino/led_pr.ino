#include <SoftwareSerial.h>

int LED = 13;
int buzzer = 12;
int cnt = 0;
int fcnt = 0;
int Tx = 6; //전송 보내는핀  
int Rx = 7; //수신 받는핀

SoftwareSerial BtSerial(Tx,Rx);

void setup() {
  // 시리얼 통신속도
  Serial.begin(9600);

  // 블루투스 통신속도 
  BtSerial.begin(9600);

  //디지털 포트 입출력 설정
  pinMode(LED, OUTPUT);
  pinMode(buzzer, OUTPUT);
}

void loop() {
  // analog 포트 세팅
  int pr = analogRead(A0);
  int ballance = analogRead(A1);
 
  // bluetooth 사용가능시 값 읽기 
  delay(100);
  if (BtSerial.available()) {       
    Serial.write(BtSerial.read());
  }
  
  // console log //
  //Serial.println(pr);
  //Serial.println(ballance);
  //Serial.println("\r\n");
  //Serial.println(cnt);
  delay(1);
  
  if(pr >= 200) //포토 레지스터(빛 감지) 조건 -> analog type
  {
    digitalWrite(LED, HIGH);
  }
  else
  {
    digitalWrite(LED, LOW);
  }

  //이벤트 발생 조건 
  if(ballance >= 700)
  {
    delay(1000);
    
    // 민감도 
    if(ballance > 725)
    {
      cnt = 1;
    }
    else
    {
      cnt = 0;
    }
  }   
 

   ///////////////////////////////////event function///////////////////////////////////////////
   if(cnt > 0)
   {
    Serial.println("-----------------event occur----------------\r\n");
    BtSerial.write("E");
    Serial.println("HELP ME!!!!!!!!!!!!!!!!!!\r\n");
    digitalWrite(buzzer, HIGH);
    delay(500);
    digitalWrite(buzzer, LOW);
    cnt = 0;
    
   }
   else
   {
    Serial.println("-----------------event wait----------------\r\n");
    BtSerial.write("0");
    digitalWrite(buzzer, LOW);
   }
    ///////////////////////////////////end function///////////////////////////////////////////
}
